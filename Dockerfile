FROM python:3.7-buster

ARG PYTHONPAD_REPO_ID
ARG PYTHONPAD_REPO_PASSWORD
ARG PYTHONPAD_REPO_TAG

ENV INSTALL_PATH /app
RUN mkdir -p $INSTALL_PATH
WORKDIR $INSTALL_PATH

# Install Git
RUN apt-get update \
    && apt-get install -y git
 
# Clone Repo
RUN git clone https://${PYTHONPAD_REPO_ID}:${PYTHONPAD_REPO_PASSWORD}@gitlab.com/intcoding/pythonpad.git .
RUN git reset --hard ${PYTHONPAD_REPO_TAG}

# Remove Git
RUN apt-get remove -y git

# Install Python dependencies
RUN pip3 install -r requirements.txt
